UID=$(shell id -u)
GID=$(shell id -g)
PHP_SERVICE := php
DOCKER_COMPOSE := docker-compose -f docker/docker-compose.yml
PHP_UNIT := ./vendor/bin/phpunit --bootstrap vendor/autoload.php --testdox
CODE_SNIFFER := ./vendor/squizlabs/php_codesniffer/bin/phpcs
CODE_SNIFFER_FIXER := ./vendor/squizlabs/php_codesniffer/bin/phpcbf
CS_FIXER := ./vendor/friendsofphp/php-cs-fixer/php-cs-fixer
CS_CONFIG := config/code-standards/php_cs.dist

start: erase cache-folders build composer-install bash

erase:
	${DOCKER_COMPOSE} down -v

build:
	${DOCKER_COMPOSE} build && \
	${DOCKER_COMPOSE} pull

cache-folders:
	mkdir -p ./.composer && chown ${UID}:${GID} ./.composer

composer-install:
	${DOCKER_COMPOSE} run --rm -u ${UID}:${GID} ${PHP_SERVICE} composer install

bash:
	${DOCKER_COMPOSE} run --rm -u ${UID}:${GID} ${PHP_SERVICE} sh

logs:
	${DOCKER_COMPOSE} logs -f ${PHP_SERVICE}

#PHPUnit:
test:
	$(DOCKER_COMPOSE) run --rm -u ${UID}:${GID} ${PHP_SERVICE} $(PHP_UNIT) --do-not-cache-result

#PHPStan:
php-stan:
	$(DOCKER_COMPOSE) run --rm -u ${UID}:${GID} ${PHP_SERVICE} vendor/bin/phpstan analyse src tests --level 6

#CodeSniffer
code-sniffer:
	$(DOCKER_COMPOSE) run --rm -u ${UID}:${GID} ${PHP_SERVICE} $(CODE_SNIFFER) --standard="config/code-standards/srcRuleset.xml" -s
code-sniffer-tests:
	$(DOCKER_COMPOSE) run --rm -u ${UID}:${GID} ${PHP_SERVICE} $(CODE_SNIFFER) --standard="config/code-standards/testsRuleset.xml" -s
code-sniffer-fix:
	$(DOCKER_COMPOSE) run --rm -u ${UID}:${GID} ${PHP_SERVICE} $(CODE_SNIFFER_FIXER)

#CS-Fixer
cs-fixer:
	$(DOCKER_COMPOSE) run --rm -u ${UID}:${GID} ${PHP_SERVICE} $(CS_FIXER) fix --config=$(CS_CONFIG) --dry-run --using-cache=no
cs-fixer-fix:
	$(DOCKER_COMPOSE) run --rm -u ${UID}:${GID} ${PHP_SERVICE} $(CS_FIXER) fix --config=$(CS_CONFIG) --using-cache=no