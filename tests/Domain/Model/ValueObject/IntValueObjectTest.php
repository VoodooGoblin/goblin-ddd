<?php

declare(strict_types=1);

namespace Goblin\Ddd\Tests\Domain\Model\ValueObject;

use PHPUnit\Framework\TestCase;

class IntValueObjectTest extends TestCase
{
    public function test_given_integer_value_when_ask_to_get_info_then_return_expected_info(): void
    {
        $int = IntValueObjectTested::from(42);

        $this->assertEquals(42, $int->value());
        $this->assertEquals(42, $int->jsonSerialize());
    }

    public function test_given_two_identical_integers_when_ask_to_check_equality_then_return_true(): void
    {
        $int = IntValueObjectTested::from(42);
        $other = IntValueObjectTested::from(42);

        $this->assertTrue($int->equalTo($other));
    }

    public function test_given_two_different_integers_when_ask_to_check_equality_then_return_false(): void
    {
        $int = IntValueObjectTested::from(42);
        $other = IntValueObjectTested::from(69);

        $this->assertFalse($int->equalTo($other));
    }
}
