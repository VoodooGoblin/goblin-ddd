<?php

declare(strict_types=1);

namespace Goblin\Ddd\Tests\Domain\Model\ValueObject;

use Goblin\Ddd\Domain\Model\ValueObject\CollectionValueObject;
use Webmozart\Assert\Assert;

class CollectionValueObjectTested extends CollectionValueObject
{
    public function add(int $item){
        return $this->addItem($item);
    }

    public function remove(int $item){
        return $this->removeItem($item);
    }

    public static function from(array $items)
    {
        Assert::allInteger($items);
        return new static($items);
    }
}
