<?php

declare(strict_types=1);

namespace Goblin\Ddd\Tests\Domain\Model\ValueObject;

use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class EnumValueObjectTest extends TestCase
{
    public function test_given_enum_class_when_ask_to_get_enums_values_then_return_array_of_values(): void
    {
        $this->assertEquals(['ENUM_1' => '1', 'ENUM_2' => '2'], EnumValueObjectTested::allowedValues());
    }

    public function test_given_available_value_when_ask_to_get_info_then_return_expected_info(): void
    {
        $enum = EnumValueObjectTested::from('1');

        $this->assertEquals('1', $enum->value());
        $this->assertEquals('1', $enum->jsonSerialize());
        $enum::allowedValues();
    }

    public function test_given_two_identical_enums_when_ask_to_check_equality_then_return_true(): void
    {
        $enum = EnumValueObjectTested::from('1');
        $other = EnumValueObjectTested::from('1');

        $this->assertTrue($enum->equalTo($other));
    }

    public function test_given_two_different_enums_when_ask_to_check_equality_then_return_false(): void
    {
        $enum = EnumValueObjectTested::from('1');
        $other = EnumValueObjectTested::from('2');

        $this->assertFalse($enum->equalTo($other));
    }

    public function test_given_invalid_enum_value_when_create_enum_then_throw_exception(): void
    {
        $this->expectException(InvalidArgumentException::class);
        EnumValueObjectTested::from('3');
    }
}
