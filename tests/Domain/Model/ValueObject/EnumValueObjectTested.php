<?php

declare(strict_types=1);

namespace Goblin\Ddd\Tests\Domain\Model\ValueObject;

use Goblin\Ddd\Domain\Model\ValueObject\EnumValueObject;

class EnumValueObjectTested extends EnumValueObject
{
    private const ENUM_1 = '1';

    private const ENUM_2 = '2';
}
