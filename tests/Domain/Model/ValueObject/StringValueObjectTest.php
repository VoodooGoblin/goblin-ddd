<?php

declare(strict_types=1);

namespace Goblin\Ddd\Tests\Domain\Model\ValueObject;

use PHPUnit\Framework\TestCase;

class StringValueObjectTest extends TestCase
{
    public function test_given_string_value_when_ask_to_get_info_then_return_expected_info(): void
    {
        $string  = uniqid();
        $stringVo = StringValueObjectTested::from($string);

        $this->assertEquals($string, $stringVo->value());
        $this->assertEquals($string, $stringVo->jsonSerialize());
    }

    public function test_given_two_identical_strings_when_ask_to_check_equality_then_return_true(): void
    {
        $string  = uniqid();
        $str = StringValueObjectTested::from($string);
        $other = StringValueObjectTested::from($string);

        $this->assertTrue($str->equalTo($other));
    }

    public function test_given_two_different_strings_when_ask_to_check_equality_then_return_false(): void
    {
        $str = StringValueObjectTested::from(uniqid());
        $other = StringValueObjectTested::from(uniqid());

        $this->assertFalse($str->equalTo($other));
    }
}
