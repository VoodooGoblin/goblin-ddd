<?php

declare(strict_types=1);

namespace Goblin\Ddd\Tests\Domain\Model\ValueObject;

use Goblin\Ddd\Domain\Model\ValueObject\FloatValueObject;

class FloatValueObjectTested extends FloatValueObject
{
}
