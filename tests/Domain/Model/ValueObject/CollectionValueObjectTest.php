<?php

declare(strict_types=1);

namespace Goblin\Ddd\Tests\Domain\Model\ValueObject;

use PHPUnit\Framework\TestCase;

class CollectionValueObjectTest extends TestCase
{
    public function test_given_empty_collection_when_ask_to_get_info_then_return_expected_info(): void
    {
        $collection = CollectionValueObjectTested::from([]);

        $this->assertEquals([], $collection->jsonSerialize());
        $this->assertTrue($collection->isEmpty());
    }

    public function test_given_collection_when_ask_to_get_info_then_return_expected_info(): void
    {
        $collection = CollectionValueObjectTested::from([1, 2, 3, 4]);

        $this->assertEquals([1, 2, 3, 4], $collection->jsonSerialize());
        $this->assertFalse($collection->isEmpty());
    }

    public function test_given_collection_when_ask_to_filter_then_return_expected_info(): void
    {
        $collection = CollectionValueObjectTested::from([1, 2, 3, 4]);
        $newCollection = $collection->filter(
            function ($current) {
                return 2  !== $current;
            }
        );

        $this->assertEquals([1, 3, 4], $newCollection->jsonSerialize());
    }

    public function test_given_collection_when_ask_to_walk_then_iterate_for_all_items(): void
    {
        $collection = CollectionValueObjectTested::from([1, 2, 3, 4]);
        $iterated = [];
        $collection->walk(
            function ($current) use (&$iterated): void {
                $iterated[] = $current;
            }
        );

        $this->assertEquals([1, 2, 3, 4], $iterated);
    }

    public function test_given_collection_when_ask_to_add_item_then_return_new_collection(): void
    {
        $collection = CollectionValueObjectTested::from([1, 2, 3, 4]);
        $newCollection = $collection->add(5);

        $this->assertEquals([1, 2, 3, 4], $collection->jsonSerialize());
        $this->assertEquals([1, 2, 3, 4, 5], $newCollection->jsonSerialize());
    }

    public function test_given_collection_when_ask_to_remove_item_then_return_new_collection(): void
    {
        $collection = CollectionValueObjectTested::from([1, 2, 3, 4]);
        $newCollection = $collection->remove(3);

        $this->assertEquals([1, 2, 3, 4], $collection->jsonSerialize());
        $this->assertEquals([1, 2, 4], $newCollection->jsonSerialize());
    }
}
