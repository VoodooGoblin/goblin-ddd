<?php

declare(strict_types=1);

namespace Goblin\Ddd\Domain\Model\ValueObject;

abstract class BoolValueObject implements ValueObject
{
    private bool $value;

    final protected function __construct(bool $value)
    {
        $this->value = $value;
    }

    final public function value(): bool
    {
        return $this->value;
    }

    final public function isTrue(): bool
    {
        return true === $this->value;
    }

    final public function isFalse(): bool
    {
        return false === $this->value;
    }

    final public function jsonSerialize(): bool
    {
        return $this->value;
    }

    final public static function from(bool $value)
    {
        return new static($value);
    }

    final public static function true()
    {
        return new static(true);
    }

    final public static function false()
    {
        return new static(false);
    }
}
