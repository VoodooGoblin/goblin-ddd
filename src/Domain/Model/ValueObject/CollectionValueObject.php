<?php

declare(strict_types=1);

namespace Goblin\Ddd\Domain\Model\ValueObject;

abstract class CollectionValueObject implements \Iterator, \Countable, ValueObject
{
    private array $items;

    final protected function __construct($items = [])
    {
        $this->items = $items;
        $this->rewind();
    }

    final protected function addItem($item): self
    {
        $items = $this->items;
        $items[] = $item;

        return new static($items);
    }

    final protected function removeItem($item): self
    {
        return $this->filter(
            static fn ($current) => $current !== $item,
        );
    }

    final public function indexOf($searchedValue)
    {
        foreach ($this->items as $key => $value) {
            if ($value === $searchedValue) {
                return $key;
            }
        }

        return false;
    }

    final public function addCollection(self $other): self
    {
        $collection = static::from($this->items);
        foreach ($other as $item) {
            $collection = $collection->add($item);
        }

        return $collection;
    }

    final protected function addArray($arr)
    {
        $collection = static::from($this->items);
        foreach ($arr as $item) {
            $collection = $collection->add($item);
        }

        return $collection;
    }

    final public function filter(callable $func)
    {
        return static::from(\array_values(\array_filter($this->items, $func)));
    }

    public function walk(callable $func): void
    {
        \array_walk($this->items, $func);
    }

    final public function isEmpty()
    {
        return empty($this->items);
    }

    final public function current()
    {
        return \current($this->items);
    }

    final public function next(): void
    {
        \next($this->items);
    }

    final public function key()
    {
        return \key($this->items);
    }

    final public function valid()
    {
        return \array_key_exists($this->key(), $this->items);
    }

    final public function rewind(): void
    {
        \reset($this->items);
    }

    final public function count()
    {
        return \count($this->items);
    }

    final public function jsonSerialize()
    {
        return $this->items;
    }
}
