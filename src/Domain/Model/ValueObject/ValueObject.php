<?php

declare(strict_types=1);

namespace Goblin\Ddd\Domain\Model\ValueObject;

use JsonSerializable;

interface ValueObject extends JsonSerializable
{
}
