<?php

declare(strict_types=1);

namespace Goblin\Ddd\Domain\Model\ValueObject;

use Ramsey\Uuid\Uuid as RamseyUuid;

class Uuid implements ValueObject
{
    private string $value;

    final protected function __construct(string $value)
    {
        $this->value = $value;
    }

    public static function from(string $value)
    {
        return new static(RamseyUuid::fromString($value)->toString());
    }

    public static function v4()
    {
        return new static(RamseyUuid::uuid4()->toString());
    }

    public function jsonSerialize()
    {
        return $this->value;
    }
}
